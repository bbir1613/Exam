import React, {Component} from "react";
import {Text, View, Navigator, TouchableOpacity, StyleSheet} from "react-native";
import {NoteList} from "./list/NoteList";
import {NoteDelete} from "./list/NoteDelete";
import {getLogger} from "./util";
const log = getLogger('Router');

export class Router extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
        log(props)
    }

    componentWillMount() {
        log("ComponentWillMount()");
    }

    render() {
        return (
            <Navigator
                initialRoute={NoteList.route}
                renderScene={this.renderScene.bind(this)}
                ref={(navigator) => this.navigator = navigator}
                navigationBar={
                    <Navigator.NavigationBar
                         style={styles.navigationBar}
                         routeMapper={NavigationBarRouteMapper}/>}
            />
        )
    };

    componentDidMount() {
        log("ComponentDidMount()");
    }

    componentWillUnmount() {
        log("ComponentWillUnmount()");
    }

    renderScene(route, navigator) {
        log(`renderScene ${route.name}`);
        switch (route.name) {
            case NoteList.routeName:
                return <NoteList
                    key={1}
                    store={this.props.store}
                    navigator={navigator}
                />;
                break;
            case NoteDelete.routeName:
                return <NoteDelete
                    key={2}
                    store={this.props.store}
                    navigator={navigator}
                />;
                break;
        }
    }
}
const NavigationBarRouteMapper = {
    LeftButton(route, navigator, index, navState) {
        if (index > 0) {
            return (<TouchableOpacity onPress={() => {
                if (route.leftAction) route.leftAction();
                if (index > 0) navigator.pop();}}>
                    <Text style={styles.leftButton}>Back</Text>
                </TouchableOpacity>
            )
        } else {
            return null;
        }
    },
    RightButton(route, navigator, index, navState) {
        if (route.rightText) return (
            <TouchableOpacity
                onPress={() => route.rightAction()}>
                <Text style={styles.rightButton}>
                    {route.rightText}
                </Text>
            </TouchableOpacity>
        )
    },
    Title(route, navigator, index, navState) {
        return (<Text style={styles.title}>{route.title}</Text>)
    }
};

const styles = StyleSheet.create({
    navigationBar: {
        backgroundColor: 'blue',
    },
    leftButton: {
        color: '#ffffff',
        margin: 10,
        fontSize: 17,
    },
    title: {
        paddingVertical: 10,
        color: '#ffffff',
        justifyContent: 'center',
        fontSize: 18
    },
    rightButton: {
        color: 'white',
        margin: 10,
        fontSize: 16
    },
    content: {
        marginTop: 90,
        marginLeft: 20,
        marginRight: 20,
    },
});
