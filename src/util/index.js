export const getLogger = (TAG: string) => (message: string) => console.log(`${TAG} - ${message}`);
export {registerRightAction} from './util'
export  {showError} from './util'

// export {serveUrl,Request} from './api'