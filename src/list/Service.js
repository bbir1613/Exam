import {getLogger} from '../util'
// const log = getLogger("EventReducer");
import {getHeaderFromConnection, createHeaders, Request, serveUrl} from "../util/api";
import {Listener} from "../util/util";
import {
    asyncStoreSaveItems,
    asyncStoreSaveLastModifyHeader,
    asyncStorageGetItemsByPage,
    asyncStorageGetItems
} from "./Database";

//apis
export const listenOnNotes = (store): Listener => {
    const log = getLogger("listenOnNotes");
    return new Listener(serveUrl, (data) => {
        log(data);
        if (data.event === "inserted") {
            log(`note inserted`);
            store.dispatch(insertNoteToState(data.note));
        }
        if (data.event === "deleted") {
            log(`note deleted`);
            store.dispatch(deleteNoteFromState(data.note));
        }
    });
};

const insertNoteToState = (note) => async(dispatch, getState) => {
    const log = getLogger("insertNoteToState");
    let items = await asyncStorageGetItems();
    items.push(note);
    log(` database items ${JSON.stringify(items)}`);
    log(` database items ${items.length}`);
    await asyncStoreSaveItems(items);
    dispatch(insertItems(items));
};

const deleteNoteFromState = (note) => async(dispatch, getState) => {
    const log = getLogger("deleteNoteFromState");
    let items = await asyncStorageGetItems();
    items = items.filter(n => n.id !== note.id);
    log(` database items ${JSON.stringify(items)}`);
    log(` database items ${items.length}`);
    await asyncStoreSaveItems(items);
    dispatch(insertItems(items));
};

export const noInternetConnection = () => async(dispatch, getState) => {
    dispatch(noInternet());
};

export const internetConnection = () => async(dispatch, getState) => {
    dispatch(internet());
};

export const deleteItem = (noteId) => async(dispatch, getState) => {
    const log = getLogger("deleteItem");
    // dispatch(startProgressIndicator());
    const request = new Request(serveUrl + 'note/' + noteId);
    request.delete().then(async res => {
        //     dispatch(stopProgressIndicator());
    }).catch(async error => {
        log('delete catch error');
        if (error.issue) dispatch(deleteIssue(error.issue));
        else dispatch(deleteIssue(error.message));
    });
};
export const loadNewPage = (page, more) => async(dispatch, getState) => {
    if (!more) return;
    const log = getLogger(`loadNewPage`);
    dispatch(startProgressIndicator());
    log(`load new page : ${page}`);
    const request = new Request(serveUrl + 'note?=' + page);
    request.get().then(async res => {
        if (res.status === 200) {
            log(`is modified`);
            let items: [] = await asyncStorageGetItems();
            log(`is modified items : ${JSON.stringify(items)}`);
            log(`is modified page : ${res.json.page}`);
            res.json.notes.forEach(note => items.push(note));
            await asyncStoreSaveItems(items);
            await asyncStoreSaveLastModifyHeader(getHeaderFromConnection(res.headers));
            // dispatch(loadItemsSucceeded(items, parseInt(res.json.page), res.json.more));
            dispatch(loadItemsSucceeded(items, page, res.json.more));
        }
        if (res.status === 304) {
            log(`Not modified`);
            const {items, page, more} = await asyncStorageGetItemsByPage(page);
            dispatch(loadItemsSucceeded(items, page, more));
        }
        dispatch(stopProgressIndicator());
    }).catch(async error => {
        log(`catch error ${error.issue}`);
        const {items, rpage, rmore} = await asyncStorageGetItemsByPage(page);
        if (error.issue) dispatch(setIssue(error.issue, items, page, more));
        else dispatch(setIssue(error.message, items, page, more));
        dispatch(stopProgressIndicator());
    });

    return request;
};

export const loadItems = () => async(dispatch, getState) => {
    const log = getLogger("loadItems");
    log(`loadNotes...`);
    dispatch(startProgressIndicator());

    const request = new Request(serveUrl + 'note');
    request.get().then(async res => {
        if (res.status === 200) {
            log(`is modified`);
            await asyncStoreSaveItems(res.json.notes);
            await asyncStoreSaveLastModifyHeader(getHeaderFromConnection(res.headers));
            dispatch(loadItemsSucceeded(res.json.notes, parseInt(res.json.page), res.json.more));
        }
        if (res.status === 304) {
            log(`Not modified`);
            const {items, page, more} = await asyncStorageGetItemsByPage(1);
            dispatch(loadItemsSucceeded(items, page, more));
        }
        dispatch(stopProgressIndicator());
    }).catch(async error => {
        log(`catch error ${error.issue}`);
        const {items, page, more} = await asyncStorageGetItemsByPage(1);
        if (error.issue) dispatch(setIssue(error.issue, items, page, more));
        else dispatch(setIssue(error.message, items, page, more));
        dispatch(stopProgressIndicator());
    });
};

//ACTIONS
const startProgressIndicator = () => {
    return {type: START_PROGRESS_INDICATOR};
};

const stopProgressIndicator = () => {
    return {type: STOP_PROGRESS_INDICATOR};
};

const loadItemsSucceeded = (items, page, more) => {
    return {type: LOAD_ITEMS_SUCCEEDED, items, page, more}
};

const insertItems = (items) => {
    return {type: INSERT_ITEMS, items}
};

const setIssue = (issue: string, items, page, more) => {
    return {type: SET_ISSUE, issue: issue, items: items, page: page, more: more};
};

const deleteIssue = (issue: string) => {
    return {type: DELETE_ISSUE, issue: issue,};
};

const noInternet = () => {
    return {type: NO_INTERNET_CONNECTION};
};

const internet = () => {
    return {type: INTERNET_CONNECTION};
};

//HANDLE ACTIONS
handleActions = {};

const START_PROGRESS_INDICATOR = "START_PROGRESS_INDICATOR";
handleActions[START_PROGRESS_INDICATOR] = (state, action) => {
    return {...state, isLoading: true, issue: undefined}
};

const LOAD_ITEMS_SUCCEEDED = "LOAD_ITEMS_SUCCEEDED";
handleActions[LOAD_ITEMS_SUCCEEDED] = (state, action) => {
    if (state.page < action.page) {
        const log = getLogger("load items succeeded");
        log(`Items : ${action.items} ${JSON.stringify(action.items)} Items length ${action.items.length}`);
        return Object.assign({}, state, {items: action.items, page: action.page, more: action.more, issue: undefined})
    }
    return {...state};
};

const INSERT_ITEMS = "INSERT_ITEMS";
handleActions[INSERT_ITEMS] = (state, action) => {
    return {...state, items: action.items, issue: undefined}
};

const STOP_PROGRESS_INDICATOR = "STOP_PROGRESS_INDICATOR";
handleActions[STOP_PROGRESS_INDICATOR] = (state, action) => {
    // return Object.assign({}, state, {isLoading: false})
    return {...state, isLoading: false};
};

const DELETE_ISSUE = "DELETE_ISSUE";
handleActions[DELETE_ISSUE] = (state, action) => {
    return {...state, issue: action.issue, isDeleting: false}
};

const SET_ISSUE = "SET_ISSUE";
handleActions[SET_ISSUE] = (state, action) => {
    const log = getLogger("set issue");
    if (state.internetConnection === false) {
        log(`no internet connection`);
        log(`no internet connection ${action.items.length} ${action.page} ${action.more}`);
        log(`no internet connection ${JSON.stringify(action.items)} ${action.page} ${action.more}`);
        return {...state, items: action.items, page: action.page, more: action.more, isLoading: false}
    }
    log(`return issue ${action.issue}`);
    return {...state, issue: action.issue, isLoading: false}
};

const NO_INTERNET_CONNECTION = "NO_INTERNET_CONNECTION";
handleActions[NO_INTERNET_CONNECTION] = (state, action) => {
    return {...state, internetConnection: false}
};

const INTERNET_CONNECTION = "INTERNET_CONNECTION";
handleActions[INTERNET_CONNECTION] = (state, action) => {
    return {...state, internetConnection: true}
};


//----------------------reducer
const initialState = {
    items: [],
    page: 0,
    more: true,
    isLoading: false,
    isDeleting: false,
    issue: undefined,
    internetConnection: true
};
export const eventReducer = (state = initialState, action) => {
    const log = getLogger("EventReducer");
    log('eventReducer');
    return handleActions[action.type] ? handleActions[action.type](state, action) : state;
};