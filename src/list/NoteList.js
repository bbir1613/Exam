import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
    ListView,
    ScrollView,
    TouchableHighlight,
    NetInfo
} from "react-native";
import {NoteView} from "./NoteView";
import styles from "../util/styles";
import {ActivityInfo} from "./ActivityInfo";
import {IssueComponent} from "./IssueComponent";
import {getLogger} from "../util";
import {loadItems, loadNewPage, internetConnection, noInternetConnection, listenOnNotes} from "./Service";
import {NoteDelete} from "./NoteDelete";

const log = getLogger('NoteList');

const LIST_ROUTE = "api/list";

const initialState = {
    items: [],
    page: 0,
    more: true,
    isLoading: true,
    issue: undefined,
    dataSource: undefined,
    internetConnection: true
};
export class NoteList extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
        this.state = initialState;
        this.store = this.props.store;
        this.contentHeight = 250;
        this.request = undefined;
    }

    static get routeName() {
        return LIST_ROUTE;
    }

    static get route() {
        return {name: LIST_ROUTE, title: "Note list"}
    }

    componentWillMount() {
        log("ComponentWillMount()");
        this.unsubscribe = this.store.subscribe(() => this.storeToState(this.store.getState().note));
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected && !this.state.internetConnection) this.store.dispatch(internetConnection());
            else if (!isConnected && this.state.internetConnection) this.store.dispatch(noInternetConnection());
        });
        this.listener = listenOnNotes(this.store);
    }

    storeToState(noteState) {
        log('storeToState');
        log(`${JSON.stringify(noteState)}`)
        const newState = Object.assign({}, this.state, {
            items: noteState.items,
            page: noteState.page,
            more: noteState.more,
            isLoading: noteState.isLoading,
            internetConnection: noteState.internetConnection,
            issue: noteState.issue
        });
        this.setState(newState, () => {
            log(`state updated ${JSON.stringify(this.state)} from store`)
        });
    }

    render() {
        const l = getLogger(`Note list  render`);
        l(` items ${JSON.stringify(this.state.items)} issue ${this.state.issue} internet ${this.state.internetConnection}`);
        l(` items length ${this.state.items.length} issue ${this.state.issue} internet ${this.state.internetConnection}`);
        const items = this.state.items === undefined ? [] : this.state.items;
        return (<View style={styles.content}>
                <ActivityInfo isLoading={this.state.isLoading}
                              issue={this.state.issue}
                              internetConnection={this.state.internetConnection}
                              page={this.state.page}
                              store={this.props.store}/>
                <IssueComponent issue={this.state.issue}
                                onPress={()=>this.store.dispatch(loadItems(this.state.page+1, this.state.more))}/>
                <View style={{height: 250}}>
                    <ScrollView style={{flex: 1}} scrollEventThrottle={1500}
                                onScroll={(event) => {this.handleScroll(event)}}
                                onContentSizeChange={(contentWidth, contentHeight)=>{log(`contentHeight ${contentHeight}`);
                                this.contentHeight =contentHeight - (25 * items.length)}}
                                pagingEnabled={true}>
                        {items.map((item, index) => <NoteView key={index} note={item}
                                                              onPress={(note) => this.onNoteClicked(note)}/>)}
                    </ScrollView>
                </View>
            </View>
        );
    };

    checkIfOnline() {
        //TODO: use NetInfo.isConnected.addEventListener()
        setInterval(() => NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected && !this.state.internetConnection) this.store.dispatch(internetConnection());
            else if (!isConnected && this.state.internetConnection) this.store.dispatch(noInternetConnection());
        }), 1000);
    }

    componentDidMount() {
        log("ComponentDidMount()");
        this.checkIfOnline();
        this.listener && this.listener.start();
    }

    componentWillUnmount() {
        // this._isMounted = false;
        log("ComponentWillUnmount()");
        this.unsubscribe();
        this.setState({...initialState}, () => {
            log(`state cleared ${JSON.stringify(this.state)}`)
        });
        this.listener && this.listener.stop();
        // this.currentRequest && this.currentRequest.cancel();
    }


    onNoteClicked(note) {
        this.props.navigator.push({...NoteDelete.route, title: 'Delete Note', rightText: 'Delete', data: note});
    }

    handleScroll(event) {
        log(`current offset : ${event.nativeEvent.contentOffset.y} contentHeight : ${this.contentHeight}`);
        log(`current offset : ${event} contentHeight : ${this.contentHeight}`);
        if (event.nativeEvent.contentOffset.y >= this.contentHeight) {
            this.request && this.request.cancel();
            this.store.dispatch(loadNewPage(this.state.page + 1, this.state.more)).then(req => {
                this.request = req;
            })
        }
    }
}
