import React, {Component} from "react";
import {Text, View, StyleSheet, TouchableHighlight} from "react-native";

export class IssueComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.issue) {
            return (
                <TouchableHighlight onPress={() => this.props.onPress()}>
                    <View>
                        <Text style={styles.listItem}>Loading to failed -error {this.props.issue} ,touch to retry</Text>
                    </View>
                </TouchableHighlight>
            );
        }
        return <Text/>
    }
}

const styles = StyleSheet.create({
    listItem: {
        margin: 5,
    }
});