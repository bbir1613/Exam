import {AsyncStorage} from 'react-native'
const ITEMS = 'items';

export const asyncStoreSaveLastModifyHeader = async(header) => await AsyncStorage.setItem('Last-Modified', JSON.stringify(header));

export const asyncStoreSaveItems = async(items) => await AsyncStorage.setItem(ITEMS, JSON.stringify(items));
export const asyncStorageGetItems = async() => {
    const items = await AsyncStorage.getItem(ITEMS);
    return JSON.parse(items);
};

export const asyncStorageGetItemsByPage = async(page) => {
    const storeItems = await asyncStorageGetItems();
    let items = [].concat(storeItems);
    for (let i = 0; i < page * 10; ++i) {
        if (i >= storeItems.length)break;
        items.push(storeItems[i]);
    }
    let more = false;
    if (page * 10 < storeItems.length) more = true;
    return {items: items, page: page, more: more};
};