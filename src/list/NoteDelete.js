import React, {Component} from "react";
import {Modal, StyleSheet, Text, TextInput, View, ActivityIndicator} from "react-native";
import styles from "../util/styles";
import {getLogger} from "../util";
import {deleteItem} from "./Service";
import {registerRightAction} from "../util/util";

const log = getLogger('NoteDelete ');

const DELETE_ROUTE = "api/delete";

export class NoteDelete extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {note: {...currentRoute.data}, isDeleting: false, issue: undefined, internetConnection: true};
        }
        this.store = this.props.store;
        registerRightAction(this.props.navigator, this.onDeleteNote.bind(this));
    }

    static get routeName() {
        return DELETE_ROUTE;
    }

    static get route() {
        return {name: DELETE_ROUTE, title: "Delete note", rightText: "Delete"}
    }

    componentWillMount() {
        log("ComponentWillMount()");
        this.unsubscribe = this.store.subscribe(() => this.storeToState(this.store.getState().note));
    }

    storeToState(noteState) {
        const newState = Object.assign({}, this.state, {
            isDeleting: noteState.isDeleting,
        });
        this.setState(newState, () => {
            log(`state updated ${JSON.stringify(this.state)} from store`)
        });
    }

    render() {
        log(`render`);
        return (
            <View style={styles.content}>
                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.isDeleting}
                    onRequestClose={()=>{}}
                >
                    <Text>Deleting please wait </Text>
                </Modal>
                <Text>{this.state.note.id}</Text>
                <Text>{this.state.note.text}</Text>
                <Text>{this.state.note.date}</Text>
            </View>
        )
    };

    componentDidMount() {
        this._isMounted = true;
        log("ComponentDidMount()");
    }

    componentWillUnmount() {
        this._isMounted = false;
        log("ComponentWillUnmount()");
        this.unsubscribe();
    }

    onDeleteNote() {
        this.store.dispatch(deleteItem(this.state.note.id)).then(res => {
            this.navigator.pop();
        });
    }
}
