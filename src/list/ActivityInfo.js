import React, {Component} from "react";
import {StyleSheet, Text, View, ActivityIndicator, ListView} from "react-native";
import styles from "../util/styles";
import {getLogger} from "../util";
import {loadItems} from "./Service";

const log = getLogger('ActivityInfo');

const LIST_ROUTE = "api/list";

export class ActivityInfo extends Component {
    constructor(props) {
        super(props);
        log('constructor()');
    }

    static get routeName() {
        return LIST_ROUTE;
    }

    static get route() {
        return {name: LIST_ROUTE, title: "Note list"}
    }

    componentWillMount() {
        log("ComponentWillMount()");
    }

    render() {
        log(`render`);
        if (this.props.isLoading) {
            return (<View>
                    <ActivityIndicator animating={this.props.isLoading}
                                       style={styles.activityIndicator} size="large"/>
                    <Text>Loading...</Text>
                </View>
            )
        }
        if (!this.props.internetConnection) {
            return <Text>Offline</Text>
        }
        return (<View/>)
    };

    componentDidMount() {
        log("ComponentDidMount()");
        this.loadNotes();
    }

    loadNotes() {
        if (this.props.page === 0) {
            this.props.store.dispatch(loadItems()).then(res => {
                if (this.props.issue) this.loadNotes();
            });
        }
    }

    componentWillUnmount() {
        log("ComponentWillUnmount()");
    }
}
